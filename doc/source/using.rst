
Using X-SOCS
============

How to use X-SOCS to reduce KMAP data, step by step.

.. seealso::
    You can have a look at the :ref:`video-tutorials` as well.

.. toctree::
    :numbered: 1
    :maxdepth: 2

    usage/starting.rst
    usage/project.rst
    usage/project_view.rst
    usage/intensity_view.rst
    usage/qspace_view.rst
    usage/results_view.rst

