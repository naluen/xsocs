
.. _project_tree:

Project tree view
-----------------

Once loaded, the main window will display the contents of the project.

.. |project_tree| image:: img/project_tree.png
.. |intensity_plot_button| image:: img/intensity_plot_button.png
.. |qspace_view_button| image:: img/qspace_view_button.png
.. |fit_results_button| image:: img/fit_results_button.png
.. |export_fit_button| image:: img/export_fit_button.png

+-------------------+
| Project tree view |
+===================+
| |project_tree|    |
+-------------------+

Acquisition group
.................

The ``Acquisition`` group contains the raw (merged) data.

Intensity group
...............

The ``Intensity`` item represents the cumulated acquired intensity.

You can open the :ref:`intensity view <intensity_view>` by clicking on the
plot button |intensity_plot_button| next to it.

QSpace group
............

The ``QSpace`` group contains all the Q space items. After a new Q space has
been created from the :ref:`intensity view <intensity_view>` it will be added
to this QSpace group.

.. _qspace_item:

QSpace item
***********

The ``QSpace`` item represents a single QSpace. It contains:

* an ``Infos`` node, which gives you the parameters that were used when converting from sample space to Q space.

* a :ref:`fit group <fit_group>` that contains all the fit results for that qspace.

You can open the :ref:`qspace view <qspace_view>` by clicking on the
button |qspace_view_button| next to it.

Qspace items can be individualy deleted by either selecting them and pressing
the delete key. They can be renamed (the actual file is not renamed) by double
clicking on them. To reset the name just set its name to an empty string.
There is also a context menu from which you can delete and rename the item,
open the folder containing the file, or copy the file path to clipboard.

.. _fit_group:

Fit group
.........

The ``fits`` group contains all the fits run on a given Q space. After a new
fit has been performed from the
:ref:`qspace view <qspace_view>` it will be added to this group.

.. _fit_item:

Fit Item
********

The ``Fit`` item represents fit results.

You can open the :ref:`fit results <fit_view>` view by clicking on the
view button |fit_results_button| next to it.

You can also export the results to a CSV file by clicking on the export button
|export_fit_button|.


Just like :ref:`Q space items <qspace_item>`, Fit items can be individualy
deleted by either selecting them and pressing the delete key. They can be
renamed (the actual file is not renamed) by double clicking on them.
To reset the name just set its name to an empty string. There is also a context
menu from which you can delete and rename the item, open the folder
containing the file, or copy the file path to clipboard.
