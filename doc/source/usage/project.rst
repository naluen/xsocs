
Loading a project
=================

The first thing to do once you have started X-SOCS is to create a project. It will contain
all the information (input and processed data) about a single sample.

From there you can either :ref:`load an existing project <load_project>`
or :ref:`create a new project <create_project>`.

.. _load_project:

Existing project
----------------

.. |open_icon| image:: img/open_icon.png

.. |open_dialog| image:: img/open_dialog.png

To load an existing project simply click on the `open project`
button |open_icon| in the toolbar or select the "`File/Open project`" menu
item. A dialog will open, which allows you to browse to the xsocs.proj file
you want to open and review its content.

+------------------------------+
| Open project dialog          |
+==============================+
| |open_dialog|                |
+------------------------------+

From there you can either choose a different file, or confirm your
choice and load the selected project. The :ref:`project tree <create_project>`
will then be populated with the project information.

.. _create_project:

New project
-----------

.. |create_icon| image:: ../../../xsocs/resources/gui/icons/create_project.png

To start the project creation wizard simmply click on the `create project` button
|create_icon| in the toolbar or select the "`File/Create project`" menu item.
The first page will ask for the folder path into which the new project will be
created.

.. warning::
    Please keep in mind that there can be only one project per folder! A
    confirmation dialog will pop up if a project file is already present in the
    chosen directory.

The next page gives you a choice between importing `SPEC` and `EDF` data
(and merge them into HDF5 files) or importing already merged HDF5 files.

#. Importing SPEC + EDF KMAP data
    X-SOCS will merge the SPEC file and the EDF images into HDF5 files.
    Please see :ref:`the page about merging KMAP SPEC and EDF data <merge_kmap>`.

#. Importing HDF5 merged data
    To load the data that has already been merged select the `master` file of
    the merged data (the file that contains links to all entries).

When this is done, X-SOCS will start summing all images. This can take a while
depending on the size of the acquired data.

Click on `Finish` to display the newly created project in the
:ref:`main window <project_tree>`.
