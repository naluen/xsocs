
.. _conversion_window:

Qspace conversion window
========================

.. |conversion_dialog| image:: img/conversion_dialog.png


+--------------------+
| Intensity view     |
+====================+
| |conversion_dialog||
+--------------------+


This window lets you configure some parameters and options for the
conversion from real space to qspace.

.. |xray_init_area| replace:: experiment.HXRD.Ang2Q
.. _xray_init_area: https://xrayutilities.sourceforge.io/xrayutilities.html#xrayutilities.experiment.HXRD.Ang2Q

+--------------------+--------------------------------------------------------+
| Parameter / option                                                          |
+====================+========================================================+
| ``Binning``        | If checked and set to anything different that [1, 1]   |
|                    | a binning will be applied to all images before         |
|                    | conversion.                                            |
|                    |                                                        |
|                    | This is the nav argument passed to                     |
|                    | xrayutilies' |xray_init_area|_                         |
+--------------------+--------------------------------------------------------+
| ``Median filter``  | If checked and set to anything else than [1, 1] then a |
|                    | median filter will be applied to all images before     |
|                    | conversion.                                            |
|                    |                                                        |
|                    | .. warning:: if non empty pixels in your images are    |
|                    |     few and far between, you might loose to much       |
|                    |     information if a median filter is applied.         |
+--------------------+--------------------------------------------------------+
| ``Grid dimensions``| dimensions of your qspace cube.                        |
+--------------------+--------------------------------------------------------+

When all is set, click on ``Convert`` to start the conversion. When the conversion
is done, the result file (hdf5) can be found at the location written in the
``output`` field.