
Starting X-SOCS
===============

X-SOCS can be started from the command line::

    python -m xsocs

Or from the Python console:

.. code-block:: python

    from xsocs.gui import xsocs_main
    xsocs_main()

This will open X-SOCS main window, with no project loaded.

+------------------------------+
| Main window                  |
+==============================+
| .. image:: img/main_view.png |
+------------------------------+

You can then load an :ref:`existing project <load_project>`
or :ref:`create a new one <create_project>`.



