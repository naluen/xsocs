
.. _qspace_view:

Fitting peaks (Q-Space view)
============================

.. |qspace_window| image:: img/qspace_window.png

+-----------------+
| Intensity view  |
+=================+
| |qspace_window| |
+-----------------+

3D view
-------

This window allows you to visualize the q space. You can display one or more
isosurfaces and a cutting plane, select a 3D ROI, and start fitting peaks.

More info about the

Isosurfaces
...........

Isosurfaces can be added/removed and configured from the top left tree widget.

Click on the ``+`` (``-``) to add (remove) an isosurface.

Move the slider or double click on the value and enter a number to change its level.

Tick (untick) the checkbox next to the isosurface color icon to show (hide) that
isosurface.

Cutting plane
.............

A cutting plane can be shown by expanding the ``Cutting plane`` node in the
plot3d parameter tree (top left of the qspace view) and ticking the ``Visible``
node checkbox.

When visible the cutting plane can be moved around with the mouse. You can switch between
`plane` and `camera` control by clicking on the desired control in the toolbar
right above the 3D view.

A vew of the isosurface is also available in the ``Cut Plane`` tab in the
bottom left corner of the window.

ROI selection
.............

The widget on the top right corner of the window allows you to select a 3D Region
Of Interest. To enable the ROI, tick the `Roi` checkbox, then move the sliders
to change the ROI ranges. The ROI ranges are displayed as white wires in the 3D
view.

Roi intensity
.............

The summed intensity inside the ROI can be visualized in the ``ROI Intensity``
tab in the bottom left corner of the window. You will have to click on the
``Update`` button as it is not refreshed in real time.

Fitting
-------

Three options are available :

* **Gaussian** : fits a simple gaussian function on the projected peak along the qx, qy, and qz axis. Results returned are:
    * area
    * center (qx, qy or qz)
    * width

* **Centroid** : computes the center of mass. Results are:
    * center of mass (qx, qy or qz)
    * intensity value closest to the center of mass
    * maximum intensity

* **Max** : just returns the maximum value and its position
    * maximum
    * position (qx, qy or qz)

After selected the desired *"fit"* type, the process can be started by clicking
on ``Run``.

When the fit is done, a new :ref:`fit item <fit_item>` is added to the
:ref:`project tree <project_tree>`. You can view the result in
the :ref:`fit view <fit_view>`.
