# coding: utf-8
# /*##########################################################################
# Copyright (C) 2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ############################################################################*/
"""
(very) Simple remote file manager. To be used to retrieve files for
    unit tests.
    Very few safeguards. Use with care.
"""

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "05/01/2017"


import os
import time
import json
import hashlib
import tempfile


try:  # Python3
    from html.parser import HTMLParser
    from urllib.request import (urlopen,
                                ProxyHandler,
                                build_opener,
                                URLError)
    from urllib.parse import urlparse
except ImportError:  # Python2
    from HTMLParser import HTMLParser
    from urllib2 import urlopen, ProxyHandler, build_opener, URLError
    from urlparse import urlparse


_HASH_BLOCKSIZE = 65536


def _get_file_hash(filename, hexdigest=True):
    """
    Computes the md5 checksum of a file.
    :param filename:
    :param hexdigest:
    :return:
    """
    hasher = hashlib.md5()
    with open(filename, 'rb') as ofile:
        buf = ofile.read(_HASH_BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = ofile.read(_HASH_BLOCKSIZE)
    if hexdigest:
        return hasher.hexdigest()
    return hasher.digest()


class _FileAnchorParser(HTMLParser):
    """
    Simple html parser to list files and folders
    """

    files = property(lambda self: self.__files)
    """ Array of file names found in the folder """

    folders = property(lambda self: self.__folders)
    """ Array of folder names found in the folder """

    def __init__(self, data):
        HTMLParser.__init__(self)
        self.__prev_attr = None
        self.__files = []
        self.__folders = []
        self.feed(data)

    def handle_starttag(self, tag, attrs):
        """
        Stores the previous start tag if there is a href attribute.
        :param tag:
        :param attrs:
        :return:
        """
        if tag == 'a':
            for key, value in attrs:
                if key == 'href':
                    self.__prev_attr = value

    def handle_endtag(self, tag):
        """
        :param tag:
        :return:
        """
        self.__prev_attr = None

    def handle_data(self, data):
        """
        If the current tag is an anchor (<a>) with href attribute, and
        data == href data then:
            if data ends with '/': it's a folder
            else: it's a file
        :param data:
        :return:
        """
        if (self.__prev_attr is not None
                and self.__prev_attr == data):
            if data.endswith('/'):
                self.__folders.append(data)
            else:
                self.__files.append(data)


class TestFilesManager(object):
    """
    (very) Simple remote file manager. To be used to retrieve files for
    unit tests.
    Very few safeguards. Use with care.

    Limitations : for now the *file://* scheme will not allow you to list
    all the files in a folder (get_files and list_files).

    manifest.md5 file expected format is the one written out by the linux
    command md5sum : <hash> *<filename>
    """

    checksum_file = 'manifest.md5'

    file_lifetime = property(lambda self: self.__file_lifetime)
    """ Delay before a file is considered too old and must be redownloaded. """

    checksum_lifetime = property(lambda self: self.__checksum_lifetime)
    """ Delay before a checksum is considered too old and must be redownloaded. """

    def __init__(self, url, cache_dir=None):
        """
        Initializes the manager with remote address *url* and local cache
        folder *cache_dir*.
        :param url:
        :param cache_dir: root folder into which the files will be stored. If
            not set (or set to None), it will look for the path in the
            XSOCS_TEST_DIR environment variable. If it is not defined,
            a temporary folder will be created.
        """
        super(TestFilesManager, self).__init__()

        url = url.rstrip('/') + '/'

        url_info = urlparse(url)

        scheme = url_info.scheme

        if not scheme:
            url = 'http://' + url

        url_info = urlparse(url)

        rel_path = url_info.path.lstrip('/')

        self.__url = url

        self.__cache_is_temp = False
        if cache_dir is None:
            cache_dir = get_env_var('XSOCS_TEST_DIR')
            if not cache_dir:
                self.__cache_is_temp = True
                cache_dir = tempfile.mkdtemp()

        netloc = url_info.netloc.replace(':', '_')

        self.__local_dir = os.path.join(cache_dir, netloc, rel_path)

        if not os.path.isdir(cache_dir):
            os.makedirs(cache_dir)

        self.__file_lifetime = None
        self.__checksum_lifetime = None

        lhf = self.__local_hash_file = os.path.join(cache_dir,
                                                    'local_hash.json')

        if not os.path.isfile(lhf):
            with open(lhf, 'w') as olhf:
                json.dump({}, olhf, indent=4)

        dictProxies = {}

        http_proxy = get_env_var('http_proxy', ignore_case=True)
        https_proxy = get_env_var('https_proxy', ignore_case=True)

        if http_proxy:
            dictProxies['http'] = os.environ["http_proxy"]
            dictProxies['https'] = os.environ["http_proxy"]

        if https_proxy:
            dictProxies['https'] = os.environ["https_proxy"]

        if dictProxies:
            proxy_handler = ProxyHandler(dictProxies)
            opener = build_opener(proxy_handler).open
        else:
            opener = urlopen

        self.__opener = opener

    def remove_file(self, path):
        """
        Removes the local copy of a file.
        :param path:
        :return:
        """

        with open(self.__local_hash_file, 'r') as olhf:
            hash_data = json.load(olhf)

        local_dir, local_file = self.__path_to_local_path(path)

        if os.path.isfile(local_file):
            os.remove(local_file)

        if path in hash_data:
            del hash_data[path]

        with open(self.__local_hash_file, 'w') as olhf:
            json.dump(hash_data, olhf, indent=4)

    def clear_files(self):
        """
        Removes all the downloaded files.
        :return:
        """

        with open(self.__local_hash_file, 'r') as olhf:
            hash_data = json.load(olhf)

        for path in hash_data:
            local_dir, local_file = self.__path_to_local_path(path)

            if os.path.isfile(local_file):
                os.remove(local_file)

            if path in hash_data:
                del hash_data[path]

        with open(self.__local_hash_file, 'w') as olhf:
            json.dump(hash_data, olhf, indent=4)

    def __read(self, relpath=None):
        """
        Reads remote data.
        :param relpath: path to be appended to the url provided to this
            TestFilesManager constructor.
        :return:
        """
        url = self.__url

        if relpath is not None:
            url += relpath

        return self.__opener(url).read()

    def __check_lifetime(self, path):
        """
        Checks if this file (or its checksum) is still considered valid.
        :param path:
        :return:
        """

        local_dir, local_file = self.__path_to_local_path(path)

        if not os.path.exists(local_file):
            return False

        if self.file_lifetime is not None:
            tdiff = time.time() - os.path.getmtime(local_file)
            if tdiff > self.file_lifetime:
                return False

        if self.checksum_lifetime is not None:
            local_hash = self.__local_hash(path)

            if local_hash is None:
                return True
            else:
                hash_time = local_hash[1]
                tdiff = time.time() - hash_time
                updated = False
                if tdiff > self.checksum_lifetime:
                    dirname = os.path.dirname(path)
                    updated = self.__update_remote_folder_hash(dirname)
                return self.__check_hash(path, reset=updated)

        return True

    def __update_remote_folder_hash(self, path=None):
        """

        :param path:
        :return:
        """
        if path is None:
            path = ''
        local_folder_hash_f = os.path.join(self.__local_dir, path)
        local_hash_f = os.path.join(local_folder_hash_f,
                                    TestFilesManager.checksum_file)

        download = False

        if os.path.isfile(local_hash_f):
            tdiff = time.time() - os.path.getmtime(local_folder_hash_f)
            if (self.checksum_lifetime is not None
                    and tdiff > self.checksum_lifetime):
                os.remove(local_hash_f)
                download = True
        else:
            download = True

        if not os.path.isdir(local_folder_hash_f):
            os.makedirs(local_folder_hash_f)

        remote_hash_file = path + '/' + TestFilesManager.checksum_file

        if download:
            try:
                hash_data = self.__read(remote_hash_file)
            except Exception:
                # TODO : proper exception filtering
                hash_data = ''

            with open(local_hash_f, "w") as olhf:
                    olhf.write(hash_data)

        return download

    def __get_remote_file_hash(self, path):
        """
        Returns the remote hash for the given file.
        :param path:
        :return:
        """
        local_dir, local_file = self.__path_to_local_path(path)

        folder, filename = os.path.split(path)
        folder_hash_f = os.path.join(local_dir,
                                     TestFilesManager.checksum_file)
        if not os.path.isfile(folder_hash_f):
            return None

        with open(folder_hash_f) as ofhf:
            line = ofhf.readline()
            while line:
                split = line.split()
                fname = split[1]
                if ((fname.startswith('*') and fname[1:] == filename)
                        or fname == filename):
                        return split[0]

                line = ofhf.readline()

        return None

    def __check_hash(self, path, reset=False):
        """
        Compares the local and remote hash for the given file. Returns True
        if the remote hash doesn't exist.
        :param path:
        :param reset: if True: recomputes the local hash
        :return:
        """
        local_hash = self.__local_hash(path, reset=reset)
        remote_hash = self.__get_remote_file_hash(path)

        if remote_hash is None:
            return True

        return local_hash[0].lower() == remote_hash.lower()

    def __local_hash(self, path, reset=False):
        """
        Returns the local hash for the given file.
        :param path:
        :param reset: if True: recomputes the hash
        :return:
        """

        local_dir, local_file = self.__path_to_local_path(path)

        with open(self.__local_hash_file, 'r') as olhf:
            hash_data = json.load(olhf)

        local_hash = hash_data.get(path)

        if reset or local_hash is None:
            file_hash = _get_file_hash(local_file)
            curtime = time.time()
            local_hash = [file_hash, curtime]
            hash_data[path] = local_hash

            with open(self.__local_hash_file, 'w') as olhf:
                json.dump(hash_data, olhf, indent=4)
        else:
            local_hash = [local_hash[0], float(local_hash[1])]

        return local_hash

    def list_files(self, relpath=None):
        """
        Returns a list of the available files found on the server at the
        given path.
        :param relpath: a path relative to the url provided when constructing
            this RemoteTestFiles instance.
        :return:
        """
        parser = _FileAnchorParser(self.__read(relpath=relpath))
        if relpath:
            relpath = relpath.rstrip('/') + '/'
            files = [relpath + rfile for rfile in parser.files]
        else:
            files = parser.files

        return files

    def list_folders(self, relpath=None):
        """
        Returns a list of the available folders found on the server at the
        given path.
        :param relpath: a path relative to the url provided when constructing
            this RemoteTestFiles instance.
        :return:
        """
        parser = _FileAnchorParser(self.__read(relpath=relpath))
        return parser.folders

    def __path_to_local_path(self, path):
        """
        Returns the local folder and local file paths relative to the
        cache directory for the given file.
        :param path:
        :return: local folder and file paths
        """
        basename, filename = os.path.split(path)

        if basename and basename != '/':
            local_dir = os.path.join(self.__local_dir,
                                     os.path.normpath(basename))
        else:
            local_dir = self.__local_dir

        return local_dir, os.path.join(local_dir, filename)

    def get_file(self, path):
        """
        Download a file from server in the give folder if it is not found at
            local_dir + url_path + path.
        :param path: path to the file. It mush be relative to the url provided
            when constructing this RemoteTestFiles instance.
        :return:
        """
        basename = os.path.dirname(path)
        local_dir, local_file = self.__path_to_local_path(path)

        self.__update_remote_folder_hash(basename)

        if not self.__check_lifetime(path):
            data = self.__read(path)

            if not os.path.isdir(local_dir):
                os.makedirs(local_dir)

            with open(local_file, "wb") as outfile:
                outfile.write(data)

            if not self.__check_hash(path, reset=True):
                raise ValueError('Local and remote hash differ for file {0}'
                                 ''.format(path))

        return local_file

    def get_files(self, path=None):
        """
        Download all files from remote folder on the server if it is not found
            at local_dir + url_path + path.
        :param path: path to the folder. It mush be relative to the url provided
            when constructing this RemoteTestFiles instance.
        :return:
        """

        if path is not None:
            path = path.rstrip('/')
        else:
            path = ''

        remote_files = self.list_files(path)

        local_files = [self.get_file(remote_file)
                       for remote_file in remote_files]

        return local_files

    def get_local_dir(self, path=None):
        """
        Returns the local path associated to a given remote path.
        :param path:
        :return:
        """

        if not path:
            path = ''
        else:
            path = os.path.normpath(path)

        return os.path.join(self.__local_dir, path)

    @checksum_lifetime.setter
    def checksum_lifetime(self, checksum_lifetime):
        """
        Sets the hash lifetime.
        :param checksum_lifetime: None or negative value will disable the
            lifetime check. Setting it to 0 means that the checksum will
            always be downloaded when get_file is called, even if the file
            already exists.
        :return:
        """
        if checksum_lifetime is not None and checksum_lifetime < 0:
            checksum_lifetime = None
        self.__checksum_lifetime = checksum_lifetime

    @file_lifetime.setter
    def file_lifetime(self, file_lifetime):
        """
        Sets the file lifetime.
        :param file_lifetime: None or negative value will disable the lifetime
            check. Setting it to 0 means that the file will always be
            downloaded when get_file is called, even if the file already
            exists.
        :return:
        """
        if file_lifetime is not None and file_lifetime < 0:
            file_lifetime = None
        self.__file_lifetime = file_lifetime


def get_env_var(name, ignore_case=False):
    """
    Returns the value of the given environment variable, or None if it is
        not defined.
    :param name:
    :param ignore_case: if True : will ignore case, and will raise an
        exception if several variables with the same name are found that have
        with different values.
    :return:
    """

    if not ignore_case:
        return os.environ.get(name)

    name_lower = name.lower()
    keys = [key for key in os.environ.keys()
            if key.lower() == name_lower]

    if len(keys) == 0:
        return None

    if len(keys) == 1:
        return os.environ[keys[0]]

    values = [os.environ[key] for key in keys]

    all_equals = values[1:] == values[:-1]

    if not all_equals:
        raise ValueError('Environment variable "{0}" is defined multiple '
                         'times with different values.'.format(name))

    return values[0]


if __name__ == '__main__':
    pass
