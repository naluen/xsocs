#!/usr/bin/python
# coding: utf8
# /*##########################################################################
#
# Copyright (c) 2015-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__date__ = "01/01/2017"
__license__ = "MIT"


import numpy as np

from .Plotter import Plotter

from ..process.fit.Fitter import Fitter
from ..process.fit.fitresults import FitStatus
from ..process.fit.sharedresults import FitSharedResults
from ..process.fit.fitresults import FitResult


class CentroidFitter(Fitter):
    def fit(self, i_fit, i_cube, qx_profile, qy_profile, qz_profile):
        profiles = qx_profile, qy_profile, qz_profile

        for iax, axis in enumerate(self._AXIS_NAMES):
            y = profiles[iax]
            Sum = y.sum()
            if Sum != 0:
                x = getattr(self, "_%s"%axis)
                com = x.dot(y) / Sum
                #idx = np.abs(x - com).argmin()
                max_idx = y.argmax()
                I_max = y[max_idx]
                x_max = x[max_idx]
                self._shared_results.set_results(axis, i_fit,
                                                 [com, Sum, I_max, x_max],
                                                 FitStatus.OK)
            else:
                self._shared_results.set_results(axis, i_fit,
                                                    [np.nan, np.nan, np.nan, np.nan],
                                                    FitStatus.FAILED)

class CentroidResults(FitSharedResults):
    def __init__(self,
                 n_points=None,
                 shared_results=None,
                 shared_status=None,
                 **kwargs):
        super(CentroidResults, self).__init__(n_points=n_points,
                                              n_params=4,
                                              n_peaks=1,
                                              shared_results=shared_results,
                                              shared_status=shared_status)

    def fit_results(self, *args, **kwargs):
        fit_name = 'Centroid'
        fitresults = FitResult(fit_name, *args, **kwargs)
        for axis in self._AXIS_NAMES:
            results = getattr(self, "_npy_%s_results"%axis)
            status  = getattr(self, "_npy_%s_status"%axis)

            for i_p, param in enumerate(("COM", "I_sum", "I_max", "Pos_max")):
                fitresults.add_result(axis, 'centroid', param, results[:, 0+i_p].ravel())

            fitresults.set_status(axis, status)

        return fitresults


class CentroidPlotter(Plotter):
    def plotFit(self, plot, x, peakParams):
        plot.setGraphTitle('center of mass')
        for peakName, peak in peakParams.items():
            center = peak.get('COM')
            xmax = peak.get('Pos_max')

            if np.isfinite(center):
                plot.addXMarker(center, legend='center of mass', text="com")
                plot.addXMarker(xmax, legend='maximum position',
                                       text="max",
                                       color="gray")

    def getPlotTitle(self):
        return 'Center Of Mass'

# process = fitH5.processes(entry)[0]
#
#     positions = fitH5.get_result(entry, process, 'COM')
#
#     plots[0].addCurve(xAcqQX, yAcqQX, legend='measured')
#     plots[0].addXMarker(positions.qx[index], legend='center of mass')
#     plots[0].setGraphTitle('QX center of mass')
#
#     plots[1].addCurve(xAcqQY, yAcqQY, legend='measured')
#     plots[1].addXMarker(positions.qy[index], legend='center of mass')
#     plots[1].setGraphTitle('QY center of mass')
#
#     plots[2].addCurve(xAcqQZ, yAcqQZ, legend='measured')
#     plots[2].addXMarker(positions.qz[index], legend='center of mass')
#     plots[2].setGraphTitle('QZ center of mass')
#


if __name__ == '__main__':
    pass
