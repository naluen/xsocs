#!/usr/bin/python
# coding: utf8
# /*##########################################################################
#
# Copyright (c) 2015-2017 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__date__ = "01/01/2017"
__license__ = "MIT"


import numpy as np

from scipy.optimize import leastsq

from .Plotter import Plotter

from ..process.fit.Fitter import Fitter
from ..process.fit.fitresults import FitStatus
from ..process.fit.sharedresults import FitSharedResults
from ..process.fit.fitresults import FitResult


# Some constants
_const_inv_2_pi_ = np.sqrt(2 * np.pi)


class GaussianFitter(Fitter):
    def __init__(self, *args, **kwargs):
        super(GaussianFitter, self).__init__(*args, **kwargs)

        self._n_peaks = self._shared_results._n_peaks

    def fit(self, i_fit, i_cube, qx_profile, qy_profile, qz_profile):
        profiles = dict(zip(self._AXIS_NAMES, 
                            [qx_profile, qy_profile, qz_profile]))
        
        for axis in self._AXIS_NAMES: # qx, qy, qz
            x = getattr(self, "_%s"%axis)
            y = profiles[axis]
            # compute guess:
            area = y.sum()/self._n_peaks * (x[-1]-x[0])/len(x)
            cen = x[y.argmax()]
            sigma = area / (y.max() * _const_inv_2_pi_)
            guess = np.tile([area, cen, sigma],self._n_peaks)
            #print(axis, guess)
            if self._n_peaks>1: # we don't actually use many peaks yet
                # a stupid way to distribute the starting values...
                idx = np.where(y>(y.max()/20.))[0]
                i_cen = idx[np.arange(0, len(idx), len(idx)//self._n_peaks)]
                cens = x[i_cen]
                guess[1::3] = cens
        
            fit, success = gaussian_fit(x, y, guess)
            self._shared_results.set_results(axis, i_fit, fit, success)


class GaussianResults(FitSharedResults):
    def __init__(self,
                 n_points=None,
                 n_peaks=1,
                 shared_results=None,
                 shared_status=None):
        super(GaussianResults, self).__init__(n_points=n_points,
                                              n_params=3,
                                              n_peaks=n_peaks,
                                              shared_results=shared_results,
                                              shared_status=shared_status)

    def fit_results(self, *args, **kwargs):
        fit_name = 'Gaussian'
        fitresults = FitResult(fit_name, *args, **kwargs)

        for axis in self._AXIS_NAMES:
            results = getattr(self, "_npy_%s_results"%axis)
            status  = getattr(self, "_npy_%s_status"%axis)
            for i_peak in range(self._n_peaks):
                peak_name = 'gauss_{0}'.format(i_peak)

                i_start = i_peak * 3 # 3 parameters
                
                for i_p, param in enumerate(("Area", "Center", "Sigma")):
                    fitresults.add_result(axis, peak_name, param,
                                          results[:, i_start+i_p].ravel())
            
            fitresults.set_status(axis, status)
        return fitresults


# 1d Gaussian func
# TODO : optimize
def gaussian(x, a, c, s):
    """
    Returns (a / (sqrt(2 * pi) * s)) * exp(- 0.5 * ((x - c) / s)^2)
    :param x: values for which the gaussian must be computed
    :param a: area under curve ( amplitude * s * sqrt(2 * pi) )
    :param c: center
    :param s: sigma
    :return: (a / (sqrt(2 * pi) * s)) * exp(- 0.5 * ((x - c) / s)^2)
    """
    # s /= _two_sqrt_2_log_2
    return (a * (1. / (_const_inv_2_pi_ * s)) *
            np.exp(-0.5 * ((x - c) / s) ** 2))


# 1d Gaussian fit
# TODO : optimize
def gaussian_fit_err(p, x, y):
    """
    :param p:
    :param x:
    :param y:
    :return:
    """
    n_p = len(p) // 3
    gSum = 0.
    for i_p in range(n_p):
        gSum += gaussian(x, *p[i_p*3:i_p*3 + 3])
    return gSum - y
    # return gaussian(x, *p) - y


_two_sqrt_2_log_2 = 2 * np.sqrt(2 * np.log(2))


def gaussian_fit(x, y, p):
    """
    Fits (leastsq) a gaussian on the provided data f(x) = y.
    p = (a, c, s)
    and f(x) = (a / (sqrt(2 * pi) * s)) * exp(- 0.5 * ((x - c) / s)^2)
    :param x:
    :param y:
    :param p:
    :return: area, position, fwhm
    """
    result = leastsq(gaussian_fit_err,
                     p,
                     args=(x, y,),
                     maxfev=100000,
                     full_output=True)

    if result[4] not in [1, 2, 3, 4]:
        return [np.nan] * len(p), FitStatus.FAILED

    return result[0], FitStatus.OK


def _gauss_first_guess(x, y):
    i_max = y.argmax()
    y_max = y[i_max]
    p1 = x[i_max]
    i_fwhm = np.where(y >= y_max / 2.)[0]
    fwhm = (x[1] - x[0]) * len(i_fwhm)
    p2 = fwhm / np.sqrt(2 * np.log(2))  # 2.35482
    p0 = y_max * np.sqrt(2 * np.pi) * p2
    return [p0, p1, p2]


class GaussianPlotter(Plotter):
    def plotFit(self, plot, x, peakParams):

        for peakName, peak in peakParams.items():
            height = peak.get('Area')
            position = peak.get('Center')
            width = peak.get('Sigma')

            params = [height, position, width]

            if np.all(np.isfinite(params)):
                fitted = gaussian(x, *params)
                plot.addCurve(x,
                              fitted,
                              legend='{0}'.format(peakName))

    def getPlotTitle(self):
        return 'Gaussian'

if __name__ == '__main__':
    pass
