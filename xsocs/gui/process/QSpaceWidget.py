# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "15/09/2016"


import numpy as np


from silx.gui import qt as Qt

from ...io.XsocsH5 import XsocsH5

from ..widgets.Containers import GroupBox, SubGroupBox
from ..widgets.Input import StyledLineEdit
from ...process.qspace.QSpaceConverter import QSpaceConverter

_ETA_LOWER = u'\u03B7'

_DEFAULT_IMG_BIN = [1, 1]

_DEFAULT_MEDFILT = [3, 3]


class ConversionParamsWidget(Qt.QWidget):
    """
    Widget for conversion parameters input :
        - qspace dimensions
        - image binning size
    """
    def __init__(self,
                 imgBinning=None,
                 medfiltDims=None,
                 **kwargs):
        super(ConversionParamsWidget, self).__init__(**kwargs)
        layout = Qt.QGridLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)

        # imgBinning = np.array(imgBinning, ndmin=1)
        # medfiltDims = np.array(medfiltDims, ndmin=1)

        # ===========
        # Image pre processing
        # ===========

        style = Qt.QApplication.instance().style()
        size = style.pixelMetric(Qt.QStyle.PM_SmallIconSize)
        icons = {True: style.standardIcon(Qt.QStyle.SP_DialogApplyButton),
                 False: style.standardIcon(Qt.QStyle.SP_DialogCancelButton)}

        imageGbox = SubGroupBox('Image processing.')
        imgGboxLayout = Qt.QGridLayout(imageGbox)

        # Binnning
        imgBinCBox = Qt.QCheckBox('1. Binning')
        self.__imgBinCBox = imgBinCBox

        inputBase = Qt.QWidget()
        inputBase.setContentsMargins(0, 0, 0, 0)
        imgBinLayout = Qt.QHBoxLayout(inputBase)
        imgBinLayout.setContentsMargins(0, 0, 0, 0)

        imgBinCBox.toggled.connect(lambda checked:
                                   imgBinCBox.setIcon(icons[checked]))
        imgBinCBox.toggled.connect(inputBase.setEnabled)
        imgBinCBox.setIconSize(Qt.QSize(size, size))

        imgBinCBox.setChecked(True)
        imgBinCBox.setChecked(False)

        imgBinHEdit = StyledLineEdit(nChar=5)
        imgBinHEdit.setValidator(Qt.QIntValidator(imgBinHEdit))
        imgBinHEdit.setAlignment(Qt.Qt.AlignRight)
        # imgBinHEdit.setText(str(imgBinning[0]))

        imgBinVEdit = StyledLineEdit(nChar=5)
        imgBinVEdit.setValidator(Qt.QIntValidator(imgBinVEdit))
        imgBinVEdit.setAlignment(Qt.Qt.AlignRight)
        # imgBinVEdit.setText(str(imgBinning[1]))

        imgBinLayout.addWidget(Qt.QLabel('w='))
        imgBinLayout.addWidget(imgBinHEdit)
        imgBinLayout.addWidget(Qt.QLabel('h='))
        imgBinLayout.addWidget(imgBinVEdit)

        imgGboxLayout.addWidget(imgBinCBox, 0, 0)
        imgGboxLayout.addWidget(inputBase, 0, 1)

        # Median filter
        medfiltCBox = Qt.QCheckBox('2. Median filter')
        self.__medfiltCBox = medfiltCBox

        inputBase = Qt.QWidget()
        inputBase.setContentsMargins(0, 0, 0, 0)
        medfiltLayout = Qt.QHBoxLayout(inputBase)
        medfiltLayout.setContentsMargins(0, 0, 0, 0)

        medfiltCBox.toggled.connect(inputBase.setEnabled)
        medfiltCBox.toggled.connect(lambda checked:
                                    medfiltCBox.setIcon(icons[checked]))
        medfiltCBox.setIconSize(Qt.QSize(size, size))

        medfiltCBox.setChecked(True)
        medfiltCBox.setChecked(False)

        medfiltHEdit = StyledLineEdit(nChar=5)
        medfiltHEdit.setValidator(Qt.QIntValidator(medfiltHEdit))
        medfiltHEdit.setAlignment(Qt.Qt.AlignRight)
        # medfiltHEdit.setText(str(medfiltDims[0]))

        medfiltVEdit = StyledLineEdit(nChar=5)
        medfiltVEdit.setValidator(Qt.QIntValidator(medfiltVEdit))
        medfiltVEdit.setAlignment(Qt.Qt.AlignRight)
        # medfiltVEdit.setText(str(medfiltDims[1]))

        medfiltLayout.addWidget(Qt.QLabel('w='))
        medfiltLayout.addWidget(medfiltHEdit)
        medfiltLayout.addWidget(Qt.QLabel('h='))
        medfiltLayout.addWidget(medfiltVEdit)

        imgGboxLayout.addWidget(medfiltCBox, 1, 0)
        imgGboxLayout.addWidget(inputBase, 1, 1)

        layout.addWidget(imageGbox)

        # ===========
        # Image pre processing
        # ===========

        qspaceGbox = SubGroupBox('QSpace grid dimensions.')
        qspaceLayout = Qt.QHBoxLayout(qspaceGbox)

        qDimsXEdit = StyledLineEdit(nChar=5)
        qDimsYEdit = StyledLineEdit(nChar=5)
        qDimsZEdit = StyledLineEdit(nChar=5)

        dimLayout = Qt.QHBoxLayout()
        qspaceLayout.addLayout(dimLayout)
        dimLayout.addWidget(Qt.QLabel('qx:'))
        dimLayout.addWidget(qDimsXEdit)
        dimLayout.addStretch(1)

        dimLayout = Qt.QHBoxLayout()
        qspaceLayout.addLayout(dimLayout)
        dimLayout.addWidget(Qt.QLabel('qy:'))
        dimLayout.addWidget(qDimsYEdit)
        dimLayout.addStretch(1)

        dimLayout = Qt.QHBoxLayout()
        qspaceLayout.addLayout(dimLayout)
        dimLayout.addWidget(Qt.QLabel('qz:'))
        dimLayout.addWidget(qDimsZEdit)
        dimLayout.addStretch(1)

        self.__imgBinHEdit = imgBinHEdit
        self.__imgBinVEdit = imgBinVEdit
        self.__medfiltHEdit = medfiltHEdit
        self.__medfiltVEdit = medfiltVEdit
        self.__qDimsXEdit = qDimsXEdit
        self.__qDimsYEdit = qDimsYEdit
        self.__qDimsZEdit = qDimsZEdit

        layout.addWidget(qspaceGbox)

        self.setImageBinning(imgBinning)
        self.setMedfiltDims(medfiltDims)

        # ===========
        # size constraints
        # ===========
        # self.setSizePolicy(Qt.QSizePolicy(Qt.QSizePolicy.Fixed,
        #                                   Qt.QSizePolicy.Fixed))

    def getMedfiltDims(self):
        """
        Returns the median filter dimensions, a 2 integers array, or None if
        the median filter is not enabled.
        :return:
        """

        if not self.__medfiltCBox.isChecked():
            return None

        hMedfilt = self.__medfiltHEdit.text()
        if len(hMedfilt) == 0:
            hMedfilt = None
        else:
            hMedfilt = int(hMedfilt)

        vMedfilt = self.__medfiltVEdit.text()
        if len(vMedfilt) == 0:
            vMedfilt = None
        else:
            vMedfilt = int(vMedfilt)
        return [hMedfilt, vMedfilt]

    def setMedfiltDims(self, medfiltDims):
        """
        Sets the median filter dimensions.
        :param medfiltDims: a 2 integers array.
        :return:
        """
        if medfiltDims is None:
            medfiltDims = (1, 1)
            medfiltDims = np.array(medfiltDims, ndmin=1)
        equal = np.array_equal(medfiltDims, [1, 1])
        self.__medfiltHEdit.setText(str(medfiltDims[0]))
        self.__medfiltVEdit.setText(str(medfiltDims[1]))
        self.__medfiltCBox.setChecked(not equal)

    def getImageBinning(self):
        """
        Returns the image binning dimensions, a 2 integers array, or None if
        the image binning is not enabled.
        :return:
        """
        if not self.__imgBinCBox.isChecked():
            return None

        h_bin = self.__imgBinHEdit.text()
        if len(h_bin) == 0:
            h_bin = None
        else:
            h_bin = int(h_bin)
        v_bin = self.__imgBinVEdit.text()
        if len(v_bin) == 0:
            v_bin = None
        else:
            v_bin = int(v_bin)
        return [h_bin, v_bin]

    def setImageBinning(self, imageBinning):
        """
        Sets the image binning dimensions.
        :param imageBinning: a 2 integers array.
        :return:
        """
        if imageBinning is None:
            imageBinning = (1, 1)
        imgBinning = np.array(imageBinning, ndmin=1)
        equal = np.array_equal(imgBinning, [1, 1])
        self.__imgBinHEdit.setText(str(imageBinning[0]))
        self.__imgBinVEdit.setText(str(imageBinning[1]))
        self.__imgBinCBox.setChecked(not equal)

    def getQspaceDims(self):
        """
        Returns the qspace dimensions, a 3 integers (> 1) array if set,
            or [None, None, None].
        :return:
        """
        qsize_x = self.__qDimsXEdit.text()
        if len(qsize_x) == 0:
            qsize_x = None
        else:
            qsize_x = int(qsize_x)
        qsize_y = self.__qDimsYEdit.text()
        if len(qsize_y) == 0:
            qsize_y = None
        else:
            qsize_y = int(qsize_y)
        qsize_z = self.__qDimsZEdit.text()
        if len(qsize_z) == 0:
            qsize_z = None
        else:
            qsize_z = int(qsize_z)
        return [qsize_x, qsize_y, qsize_z]

    def setQSpaceDims(self, qspaceDims):
        """
        Sets the qspace dimensions.
        :param qspaceDims: A three integers array.
        :return:
        """
        self.__qDimsXEdit.setText(str(int(qspaceDims[0])))
        self.__qDimsYEdit.setText(str(int(qspaceDims[1])))
        self.__qDimsZEdit.setText(str(int(qspaceDims[2])))


class QSpaceWidget(Qt.QDialog):
    sigProcessDone = Qt.Signal(object)

    (StatusUnknown, StatusInit,
     StatusRunning, StatusCompleted,
     StatusAborted, StatusCanceled) = StatusList = range(6)

    __sigConvertDone = Qt.Signal()

    def __init__(self,
                 xsocH5File,
                 outQSpaceH5,
                 qspaceDims=None,
                 imageBinning=None,
                 medfiltDims=None,
                 roi=None,
                 entries=None,
                 shiftH5File=None,
                 **kwargs):
        """
        Widgets displaying informations about data to be converted to QSpace,
            and allowing the user to input some parameters.
        :param xsocH5File: name of the input XsocsH5 file.
        :param outQSpaceH5: name of the output hdf5 file
        :param qspaceDims: dimensions of the qspace volume
        :param imageBinning: binning to apply to the images before conversion.
            Default : (1, 1)
        :param medfiltDims: dimensions of the kernel used when applying a
            a median filter to the images (after the binning, if any).
            Set to None or (1, 1) to disable the median filter.
        :param roi: Roi in sample coordinates (xMin, xMax, yMin, yMax)
        :param entries: a list of entry names to convert to qspace. If None,
            all entries found in the xsocsH5File will be used.
        :param shiftH5File: name of a ShiftH5 file to use.
        :param kwargs:
        """
        super(QSpaceWidget, self).__init__(**kwargs)

        self.__status = QSpaceWidget.StatusInit

        xsocsH5 = XsocsH5(xsocH5File)

        # checking entries
        if entries is None:
            entries = xsocsH5.entries()
        elif len(entries) == 0:
            raise ValueError('At least one entry must be selected.')
        else:
            diff = set(entries) - set(xsocsH5.entries())
            if len(diff) > 0:
                raise ValueError('The following entries were not found in '
                                 'the input file :\n - {0}'
                                 ''.format('\n -'.join(diff)))

        self.__converter = QSpaceConverter(xsocH5File,
                                           output_f=outQSpaceH5,
                                           qspace_dims=qspaceDims,
                                           img_binning=imageBinning,
                                           medfilt_dims=medfiltDims,
                                           roi=roi,
                                           entries=entries,
                                           shiftH5_f=shiftH5File)

        self.__params = {'roi': roi,
                         'xsocsH5_f': xsocH5File,
                         'qspaceH5_f': outQSpaceH5}
        
        topLayout = Qt.QGridLayout(self)

        # ATTENTION : this is done to allow the stretch
        # of the QTableWidget containing the scans info
        topLayout.setColumnStretch(1, 1)

        # ################
        # input QGroupBox
        # ################

        inputGbx = GroupBox("Input")
        layout = Qt.QHBoxLayout(inputGbx)
        topLayout.addWidget(inputGbx,
                            0, 0,
                            1, 2)

        # data HDF5 file input
        lab = Qt.QLabel('XsocsH5 file :')
        xsocsFileEdit = StyledLineEdit(nChar=50, readOnly=True)
        xsocsFileEdit.setText(xsocH5File)
        layout.addWidget(lab,
                         stretch=0,
                         alignment=Qt.Qt.AlignLeft)
        layout.addWidget(xsocsFileEdit,
                         stretch=0,
                         alignment=Qt.Qt.AlignLeft)
        layout.addStretch()

        # ################
        # Scans
        # ################
        scansGbx = GroupBox("Scans")
        topLayout.addWidget(scansGbx, 1, 1, 2, 1)
        topLayout.setRowStretch(2, 1000)

        grpLayout = Qt.QVBoxLayout(scansGbx)
        infoLayout = Qt.QGridLayout()
        grpLayout.addLayout(infoLayout)
        #

        line = 0

        style = Qt.QApplication.instance().style()
        shiftLayout = Qt.QHBoxLayout()
        if shiftH5File is not None:
            shiftText = 'Shift applied.'
            icon = Qt.QStyle.SP_MessageBoxWarning
        else:
            shiftText = 'No shift applied.'
            icon = Qt.QStyle.SP_MessageBoxInformation
        shiftLabel = Qt.QLabel(shiftText)
        size = style.pixelMetric(Qt.QStyle.PM_ButtonIconSize)
        shiftIcon = Qt.QLabel()
        icon = style.standardIcon(icon)
        shiftIcon.setPixmap(icon.pixmap(size))
        shiftLayout.addWidget(shiftIcon)
        shiftLayout.addWidget(shiftLabel)
        infoLayout.addLayout(shiftLayout, line, 0)

        line = 1
        label = Qt.QLabel('# Roi :')
        self.__roiXMinEdit = xMinText = StyledLineEdit(nChar=5, readOnly=True)
        self.__roiXMaxEdit = xMaxText = StyledLineEdit(nChar=5, readOnly=True)
        self.__roiYMinEdit = yMinText = StyledLineEdit(nChar=5, readOnly=True)
        self.__roiYMaxEdit = yMaxText = StyledLineEdit(nChar=5, readOnly=True)
        roiLayout = Qt.QHBoxLayout()
        roiLayout.addWidget(xMinText)
        roiLayout.addWidget(xMaxText)
        roiLayout.addWidget(yMinText)
        roiLayout.addWidget(yMaxText)
        infoLayout.addWidget(label, line, 0)
        infoLayout.addLayout(roiLayout, line, 1, alignment=Qt.Qt.AlignLeft)

        line += 1
        label = Qt.QLabel('# points :')
        self.__nImgLabel = nImgLabel = StyledLineEdit(nChar=16, readOnly=True)
        nImgLayout = Qt.QHBoxLayout()
        infoLayout.addWidget(label, line, 0)
        infoLayout.addLayout(nImgLayout, line, 1, alignment=Qt.Qt.AlignLeft)
        nImgLayout.addWidget(nImgLabel)
        nImgLayout.addWidget(Qt.QLabel(' (roi / total)'))

        line += 1
        label = Qt.QLabel(u'# {0} :'.format(_ETA_LOWER))
        self.__nAnglesLabel = nAnglesLabel = StyledLineEdit(nChar=5,
                                                            readOnly=True)
        infoLayout.addWidget(label, line, 0)
        infoLayout.addWidget(nAnglesLabel, line, 1, alignment=Qt.Qt.AlignLeft)
        infoLayout.setColumnStretch(2, 1)

        self.__scansTable = scansTable = Qt.QTableWidget(0, 2)
        scansTable.verticalHeader().hide()
        grpLayout.addWidget(scansTable, alignment=Qt.Qt.AlignLeft)

        # ################
        # conversion params
        # ################

        convGbx = GroupBox("Conversion parameters")
        grpLayout = Qt.QVBoxLayout(convGbx)
        topLayout.addWidget(convGbx, 1, 0, alignment=Qt.Qt.AlignTop)

        imgBinning = self.__converter.image_binning
        medfiltDims = self.__converter.medfilt_dims
        self.__paramsWid = paramsWid = ConversionParamsWidget(
            imgBinning=imgBinning, medfiltDims=medfiltDims)
        grpLayout.addWidget(paramsWid)

        # ################
        # output
        # ################

        outputGbx = GroupBox("Output")
        layout = Qt.QHBoxLayout(outputGbx)
        topLayout.addWidget(outputGbx, 3, 0, 1, 2)
        lab = Qt.QLabel('Output :')
        outputFileEdit = StyledLineEdit(nChar=50, readOnly=True)
        outputFileEdit.setText(outQSpaceH5)
        layout.addWidget(lab,
                         stretch=0,
                         alignment=Qt.Qt.AlignLeft)
        layout.addWidget(outputFileEdit,
                         stretch=0,
                         alignment=Qt.Qt.AlignLeft)
        layout.addStretch()

        # ################
        # buttons
        # ################

        self.__converBn = convertBn = Qt.QPushButton('Convert')
        cancelBn = Qt.QPushButton('Cancel')
        hLayout = Qt.QHBoxLayout()
        topLayout.addLayout(hLayout, 4, 0, 1, 2,
                            Qt.Qt.AlignHCenter | Qt.Qt.AlignTop)
        hLayout.addWidget(convertBn)
        hLayout.addWidget(cancelBn)

        # #################
        # setting initial state
        # #################

        cancelBn.clicked.connect(self.close)
        convertBn.clicked.connect(self.__slotConvertBnClicked)

        self.__fillScansInfos()

    def __slotConvertBnClicked(self):
        """
        Slot called when the convert button is clicked. Does some checks
        then starts the conversion if all is OK.
        :return:
        """
        converter = self.__converter
        if converter is None:
            # shouldn't be here
            raise RuntimeError('Converter not found.')
        elif converter.is_running():
            # this part shouldn't even be called, just putting this
            # in case someone decides to modify the code to enable the
            # convert_bn even tho conditions are not met.
            Qt.QMessageBox.critical(self, 'Error',
                                    'A conversion is already in progress!')
            return

        output_file = converter.output_f

        if len(output_file) == 0:
            Qt.QMessageBox.critical(self, 'Error',
                                    'Output file field is mandatory.')
            return

        imageBinning = self.__paramsWid.getImageBinning()
        medfiltDims = self.__paramsWid.getMedfiltDims()
        qspaceDims = self.__paramsWid.getQspaceDims()

        try:
            converter.image_binning = imageBinning
            converter.medfilt_dims = medfiltDims
            converter.qspace_dims = qspaceDims
        except ValueError as ex:
            Qt.QMessageBox.critical(self, 'Error',
                                    str(ex))
            return

        errors = converter.check_parameters()
        if errors:
            msg = 'Invalid parameters.\n{0}'.format('\n'.join(errors))
            Qt.QMessageBox.critical(self, 'Error', msg)
            return

        if len(converter.check_overwrite()):
            ans = Qt.QMessageBox.warning(self,
                                         'Overwrite?',
                                         ('The output file already exists.'
                                          '\nDo you want to overwrite it?'),
                                         buttons=Qt.QMessageBox.Yes |
                                         Qt.QMessageBox.No)
            if ans == Qt.QMessageBox.No:
                return

        self.__converter = converter
        procDialog = _ConversionProcessDialog(converter, parent=self)
        procDialog.accepted.connect(self.__slotConvertDone)
        procDialog.rejected.connect(self.__slotConvertDone)
        self._setStatus(self.StatusRunning)
        rc = procDialog.exec_()

        if rc == Qt.QDialog.Accepted:
            self.__slotConvertDone()
        procDialog.deleteLater()

    def __slotConvertDone(self):
        """
        Method called when the conversion has been completed succesfuly.
        :return:
        """
        converter = self.__converter
        if not converter:
            return

        self.__qspaceH5 = None
        status = converter.status

        if status == QSpaceConverter.DONE:
            self.__qspaceH5 = converter.results
            self._setStatus(self.StatusCompleted)
            self.hide()
            self.sigProcessDone.emit(self.__qspaceH5)
        elif status == QSpaceConverter.CANCELED:
            self._setStatus(self.StatusAborted)
        else:
            self._setStatus(self.StatusUnknown)

    qspaceH5 = property(lambda self: self.__qspaceH5)
    """ Written file (set when the conversion was succesful, None otherwise. """

    status = property(lambda self: self.__status)
    """ Status of the widget. """

    def _setStatus(self, status):
        """
        Sets the status of the widget.
        :param status:
        :return:
        """
        if status not in QSpaceWidget.StatusList:
            raise ValueError('Unknown status value : {0}.'
                             ''.format(status))
        self.__status = status

    def __fillScansInfos(self):
        """
        Fills the QTableWidget with info found in the input file
        """
        converter = self.__converter
        if converter is None:
            return

        scans = converter.scans
        scansTable = self.__scansTable
        scansTable.setRowCount(len(scans))
        for row, scan in enumerate(scans):
            params = converter.scan_params(scan)
            item = Qt.QTableWidgetItem(scan)
            item.setFlags(item.flags() ^ Qt.Qt.ItemIsEditable)
            scansTable.setItem(row, 0, item)
            item = Qt.QTableWidgetItem(str(params['angle']))
            item.setFlags(item.flags() ^ Qt.Qt.ItemIsEditable)
            scansTable.setItem(row, 1, item)

        scansTable.resizeColumnsToContents()
        width = (sum([scansTable.columnWidth(i)
                     for i in range(scansTable.columnCount())]) +
                 scansTable.verticalHeader().width() +
                 20)
        # TODO : the size is wrong when the
        # verticalScrollBar isnt displayed yet
        # scans_table.verticalScrollBar().width())
        size = scansTable.minimumSize()
        size.setWidth(width)
        scansTable.setMinimumSize(size)

        # TODO : warning if the ROI is empty (too small to contain images)
        params = converter.scan_params(scans[0])
        roi = converter.roi
        if roi is None:
            xMin = xMax = yMin = yMax = 'ns'
        else:
            xMin, xMax, yMin, yMax = roi

        self.__roiXMinEdit.setText(str(xMin))
        self.__roiXMaxEdit.setText(str(xMax))
        self.__roiYMinEdit.setText(str(yMin))
        self.__roiYMaxEdit.setText(str(yMax))

        indices = converter.sample_indices
        nImgTxt = '{0} / {1}'.format(len(indices),
                                     params['n_images'])
        self.__nImgLabel.setText(nImgTxt)

        nEntries = len(XsocsH5(self.__params['xsocsH5_f']).entries())
        self.__nAnglesLabel.setText('{0} / {1}'.format(len(scans), nEntries))


class _ConversionProcessDialog(Qt.QDialog):
    __sigConvertDone = Qt.Signal()

    def __init__(self, converter,
                 parent=None,
                 **kwargs):
        """
        Simple widget displaying a progress bar and a info label during the
            conversion process.
        :param converter:
        :param parent:
        :param kwargs:
        """
        super(_ConversionProcessDialog, self).__init__(parent)
        layout = Qt.QVBoxLayout(self)

        progress_bar = Qt.QProgressBar()
        layout.addWidget(progress_bar)
        status_lab = Qt.QLabel('<font color="blue">Conversion '
                               'in progress</font>')
        status_lab.setFrameStyle(Qt.QFrame.Panel | Qt.QFrame.Sunken)
        layout.addWidget(status_lab)

        bn_box = Qt.QDialogButtonBox(Qt.QDialogButtonBox.Abort)
        layout.addWidget(bn_box)
        bn_box.accepted.connect(self.accept)
        bn_box.rejected.connect(self.__onAbort)

        self.__sigConvertDone.connect(self.__convertDone)

        self.__bn_box = bn_box
        self.__progress_bar = progress_bar
        self.__status_lab = status_lab
        self.__converter = converter
        self.__aborted = False

        self.__qtimer = Qt.QTimer()
        self.__qtimer.timeout.connect(self.__onProgress)

        converter.convert(blocking=False,
                          overwrite=True,
                          callback=self.__sigConvertDone.emit,
                          **kwargs)

        self.__qtimer.start(1000)

    def __onAbort(self):
        """
        Slot called when the abort button is clicked.
        :return:
        """
        self.__status_lab.setText('<font color="orange">Cancelling...</font>')
        self.__bn_box.button(Qt.QDialogButtonBox.Abort).setEnabled(False)
        self.__converter.abort(wait=False)
        self.__aborted = True

    def __onProgress(self):
        """
        Slot called when the progress timer timeouts.
        :return:
        """
        progress = self.__converter.progress()
        self.__progress_bar.setValue(progress)

    def __convertDone(self):
        """
        Callback called when the conversion is done (whether its successful or
        not).
        :return:
        """
        self.__qtimer.stop()
        self.__qtimer = None
        self.__onProgress()
        abortBn = self.__bn_box.button(Qt.QDialogButtonBox.Abort)

        converter = self.__converter

        if converter.status == QSpaceConverter.CANCELED:
            self.__bn_box.rejected.disconnect(self.__onAbort)
            self.__status_lab.setText('<font color="red">Conversion '
                                      'cancelled.</font>')
            abortBn.setText('Close')
            self.__bn_box.rejected.connect(self.reject)
            abortBn.setEnabled(True)
        elif converter.status == QSpaceConverter.ERROR:
            self.__bn_box.removeButton(abortBn)
            okBn = self.__bn_box.addButton(Qt.QDialogButtonBox.Ok)
            self.__status_lab.setText('<font color="red">Error : {0}.</font>'
                                      ''.format(converter.status_msg))
            okBn.setText('Close')
        else:
            self.__bn_box.removeButton(abortBn)
            okBn = self.__bn_box.addButton(Qt.QDialogButtonBox.Ok)
            self.__status_lab.setText('<font color="green">Conversion '
                                      'done.</font>')
            okBn.setText('Close')

    status = property(lambda self: 0 if self.__aborted else 1)
    """ Status of the process. """


if __name__ == '__main__':
    pass
