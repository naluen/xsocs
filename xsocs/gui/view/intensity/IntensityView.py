# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "15/09/2016"

import os

from collections import namedtuple

import numpy as np

from silx.gui import qt as Qt

from .PlotModel import PlotTree
from .IntensityModel import IntensityTree, IntensityGroupNode
from .RectRoiWidget import RectRoiWidget

from ..shift.ShiftView import ShiftWidget

from ...model.ModelDef import ModelRoles
from ...widgets.XsocsPlot2D import XsocsPlot2D
from ...widgets.Buttons import FixedSizePushButon


IntensityViewEvent = namedtuple('IntensityViewEvent', ['roi',
                                                       'entries',
                                                       'shiftFile'])


class IntensityView(Qt.QMainWindow):
    sigProcessApplied = Qt.Signal(object)

    plot = property(lambda self: self.__plotWindow)

    def __init__(self,
                 intensityGroup,
                 parent=None,
                 **kwargs):
        """
        Widget displaying the summed intensities over the sample. It allows
        for angle selection and ROI selection.
        :param intensityGroup: IntensityGroup instance.
        :param parent: parent widget.
        :param kwargs:
        """
        super(IntensityView, self).__init__(parent, **kwargs)

        # =================================
        # TODO : this is temporary code to allow previously created projects
        # to work with the new shift
        # TODO : get prefix
        xsocsProject = intensityGroup.projectRoot()
        sGroup = xsocsProject.shiftGroup()
        sGroup._fixFile()
        # =================================

        self.setWindowTitle('[XSOCS] {0}:{1}'.format(intensityGroup.filename,
                                                     intensityGroup.path))

        self.__displayedNode = None
        self.__selectedPoint = None
        self.__shiftApplied = False

        self.__plotWindow = plotWindow = XsocsPlot2D()
        plotWindow.setShowMousePosition(True)
        plotWindow.setShowSelectedCoordinates(True)
        plotWindow.sigPointSelected.connect(self.__slotPointSelected)

        selector = Qt.QWidget()
        layout = Qt.QVBoxLayout(selector)

        shiftLayout = Qt.QHBoxLayout()
        shiftCb = Qt.QCheckBox('[WIP] Apply shift')
        shiftEditBn = Qt.QToolButton()
        shiftEditBn.setText('Edit')
        shiftEditBn.setToolTip('Edit shift values')
        shiftLayout.addWidget(shiftCb)
        shiftLayout.addWidget(shiftEditBn)

        shiftCb.toggled.connect(self.__slotShiftToggled)
        shiftEditBn.clicked.connect(self.__slotShiftEditClicked)

        layout.addLayout(shiftLayout)

        # TODO : check item type
        self.__iGroup = intensityGroup
        self.__tree = tree = IntensityTree(intensityGroup,
                                           parent=self)
        tree.model().dataChanged.connect(self.__slotModelDataChanged)
        tree.sigCurrentChanged.connect(self.__slotItemSelected)
        layout.addWidget(tree)

        bnLayout = Qt.QHBoxLayout()
        selAllBn = FixedSizePushButon('Select All')
        selNoneBn = FixedSizePushButon('Unselect All')
        selAllBn.clicked.connect(tree.model().checkAll)
        selNoneBn.clicked.connect(tree.model().uncheckAll)
        bnLayout.addWidget(selAllBn)
        bnLayout.addWidget(selNoneBn)
        layout.addLayout(bnLayout)

        dock = Qt.QDockWidget(self)
        dock.setWidget(selector)
        features = dock.features() ^ Qt.QDockWidget.DockWidgetClosable
        dock.setFeatures(features)
        self.addDockWidget(Qt.Qt.LeftDockWidgetArea, dock)

        rectRoiWidget = RectRoiWidget(plot=plotWindow)

        self.__roiManager = rectRoiWidget.roiManager()

        applyBn = rectRoiWidget.applyButton()
        applyBn.setText('To Q Space')
        applyBn.setToolTip('To Q Space')
        rectRoiWidget.sigRoiApplied.connect(self.__slotRoiApplied)

        dock = Qt.QDockWidget(self)
        dock.setWidget(rectRoiWidget)
        features = dock.features() ^ Qt.QDockWidget.DockWidgetClosable
        dock.setFeatures(features)
        self.addDockWidget(Qt.Qt.RightDockWidgetArea, dock)

        profileWid = Qt.QWidget()
        profileLayout = Qt.QHBoxLayout(profileWid)

        self.__profilePlot = profilePlot = XsocsPlot2D()
        profilePlot.setKeepDataAspectRatio(False)
        profileLayout.addWidget(profilePlot, 10)

        plotTree = PlotTree(profilePlot)
        profileLayout.addWidget(plotTree)

        dock = Qt.QDockWidget(self)
        dock.setWidget(profileWid)
        features = dock.features() ^ Qt.QDockWidget.DockWidgetClosable
        dock.setFeatures(features)
        self.addDockWidget(Qt.Qt.BottomDockWidgetArea, dock)

        self.setCentralWidget(plotWindow)

        iGroupIndex = tree.model().iGroupNodeIndex()
        tree.selectionModel().setCurrentIndex(iGroupIndex,
                                     Qt.QItemSelectionModel.ClearAndSelect)

    def __slotShiftToggled(self, checked):
        """
        Slot called when the 'apply shift' checkbox is toggled.
        :param checked:
        :return:
        """
        self.__shiftApplied = checked
        tree = self.__tree
        model = tree.model()
        if checked:
            sItems = self.__iGroup.projectRoot().shiftGroup().getShiftItems()
            if len(sItems) > 1:
                raise NotImplementedError('Only one shift item supported '
                                          'right now.')
            sItem = sItems[0]
        else:
            sItem = None
        # at the moment a shift item is created the first time the project
        # is created
        # couldnt set the curstor with QWidget.setCursor for some reason
        Qt.QApplication.instance().setOverrideCursor(Qt.Qt.WaitCursor)
        model.setShiftItem(sItem)

        indexes = self.__tree.selectedIndexes()
        if len(indexes) == 0:
            index = model.index(0,
                                0,
                                tree.rootIndex())
            tree.selectionModel().select(
                index,
                Qt.QItemSelectionModel.ClearAndSelect)
        else:
            # only one index is selectable
            index = indexes[0]

        self.__slotItemSelected(index.data(ModelRoles.InternalDataRole))

        Qt.QApplication.instance().restoreOverrideCursor()

    def __slotShiftEditClicked(self):
        """
        Slot called when the 'edit shift' button is clicked.
        :return:
        """
        # TODO : only one shift item supported at the moment
        shiftItem = self.__iGroup.projectRoot().shiftGroup().getShiftItems()[0]
        shiftWidget = ShiftWidget(self.__iGroup, shiftItem, parent=self)
        shiftWidget.setAttribute(Qt.Qt.WA_DeleteOnClose)
        shiftWidget.setWindowModality(Qt.Qt.WindowModal)
        shiftWidget.show()

    def __slotModelDataChanged(self, topLeft, bottomRight, roles=None):
        """
        Slot called when data changes in the model. In this case it is
        when an entry is checked or unchecked.
        :param topLeft:
        :param bottomRight:
        :param roles:
        :return:
        """
        nodeL = topLeft.data(ModelRoles.InternalDataRole)
        nodeR = bottomRight.data(ModelRoles.InternalDataRole)

        if nodeL != nodeR:
            print('Multiple selection not supported yet.')
            return

        if nodeL is None:
            return

        if not isinstance(nodeL, IntensityGroupNode):
            return
        # else: the total intensity has changed.

        self.__slotPointSelected(self.__selectedPoint)

        if nodeL == self.__displayedNode:
            self.__slotItemSelected(nodeL)

    def __slotItemSelected(self, node):
        """
        Slot called when an item is selected in the tree view. Updates the
        scatter plot accordingly.
        :param node:
        :return:
        """
        self.__displayedNode = node
        intensity, positions = node.scatterData()
        title = node.nodeName

        if intensity is None:
            self.__plotWindow.clear()
            self.__plotWindow.setGraphTitle(title)
            return

        self.setPlotData(positions[0], positions[1], intensity, title)

    def setPlotData(self, x, y, data, title=None):
        """
        Displays the scatter plot.
        :param x:
        :param y:
        :param data:
        :param title:
        :return:
        """
        plot = self.__plotWindow
        plot.setPlotData(x, y, data)
        plot.setGraphTitle(title)

    def __slotPointSelected(self, point):
        """
        Slot called when a point is selected on the intensity map.
        :param point:
        :return:
        """
        plot = self.__profilePlot
        plot.setGraphTitle()
        plot.clear()

        if point is None:
            return

        iGroup = self.__iGroup
        entries, selected, unselected =\
            self.__tree.model().getCheckedEntries()
        nEntries = len(entries)

        xsocsH5 = iGroup.projectRoot().xsocsH5

        angles = np.ndarray(shape=(nEntries, ))
        intensities = np.ndarray(shape=(nEntries,))
        # TODO : error if selected not in iItems (isnt supposed to happen...)
        # TODO : make sure the angles are sorted?
        for entryIdx, entry in enumerate(entries):
            item = iGroup.getIntensityItem(entry)
            intensities[entryIdx] = item.getPointValue(point.xIdx)
            angles[entryIdx] = xsocsH5.scan_angle(entry)

        title = 'Profile @ ({0:.7g}, {1:.7g})'.format(point.x, point.y)
        plot.addCurve(angles, intensities, legend='All')
        plot.setGraphTitle(title)

        if selected:
            plot.addCurve(angles[selected],
                          intensities[selected],
                          legend='Selected ({0}/{1}'.format(len(selected),
                                                            len(entries)))

        for unselIdx in unselected:
            plot.addXMarker(angles[unselIdx],
                            legend=entries[unselIdx],
                            color='red')

        self.__selectedPoint = point

    def __slotRoiApplied(self, roi):
        """
        Slot called when the ROI is applied.
        :param roi:
        :return:
        """
        entries, selected, _ =\
            self.__tree.model().getCheckedEntries()

        selEntries = [entries[idx] for idx in selected]

        if self.__shiftApplied:
            shiftGroup = self.__iGroup.projectRoot().shiftGroup()
            # TODO : only one shift file supported right now
            shiftFile = shiftGroup.getShiftItems()[0].shiftFile
        else:
            shiftFile = None
        event = IntensityViewEvent(roi=roi,
                                   entries=selEntries,
                                   shiftFile=shiftFile)

        self.sigProcessApplied.emit(event)


if __name__ == '__main__':
    pass
