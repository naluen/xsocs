# coding: utf-8
# /*##########################################################################
#
# Copyright (c) 2015-2016 European Synchrotron Radiation Facility
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
# ###########################################################################*/

from __future__ import absolute_import

__authors__ = ["D. Naudet"]
__license__ = "MIT"
__date__ = "15/09/2016"


from silx.gui import qt as Qt


from ...model.TreeView import TreeView
from ...model.ModelDef import ModelRoles
from ...model.Model import Model, RootNode, Node


class IntensityViewItemNode(Node):
    checkable = True

    item = property(lambda self: self.__item)

    def __init__(self, iItem=None, **kwargs):
        super(IntensityViewItemNode, self).__init__(**kwargs)

        # TODO : check item type
        # TODO : not satisfactory, improve= self.subject.getIntensityItem(entry)
        if not iItem:
            iItem = self.subject.getIntensityItem(self.nodeName)

        self.__item = iItem
        shortName = iItem.projectRoot().shortName(iItem.entry)
        if not shortName:
            shortName = iItem.path.split('/')[-1]

        self.nodeName = str(shortName)

        self.__shiftItem = None

        self._setDataInternal(IntensityModelColumns.AngleColumn,
                              iItem.entry,
                              Qt.Qt.ToolTipRole)

    def scatterData(self):
        data, pos = self.item.getScatterData()
        if not self.__shiftItem:
            return data, (pos.pos_0, pos.pos_1)

        shiftH5 = self.__shiftItem.shiftH5
        with shiftH5:
            shift = shiftH5.shift(self.item.entry)
            if shift['shift_x'] is not None and shift['shift_y'] is not None:
                shift_idx = shiftH5.shifted_indices(self.item.entry)
            else:
                shift_idx = None

        if shift_idx is not None:
            pos0 = pos.pos_0[shift_idx]
            pos1 = pos.pos_1[shift_idx]
            data = data[shift_idx]
        else:
            pos0 = pos.pos_0
            pos1 = pos.pos_1

        return data, (pos0, pos1)

    def setShiftItem(self, shiftItem=None):
        """
        Sets the shift item to be used by this node and its children.
        The contents of the items are not checked.
        :param shiftItem: an instance of ShiftItem, or None to clear the
        existing one.
        :return:
        """
        self.__shiftItem = shiftItem

        if shiftItem is None:
            self._setDataInternal(IntensityModelColumns.ShiftColumn, None)
            return

        # TODO : temporary.
        if self.__item.entry == 'Total':
            return

        shiftH5 = shiftItem.shiftH5
        with shiftH5:
            shift = shiftH5.shift(self.item.entry)
            shiftX = shift['shift_x']
            shiftY = shift['shift_y']
            gridShift = shift['grid_shift']

            if shiftX is None and shiftY is None:
                text = 'Not set'
            elif shiftX is None or shiftY is None:
                # error
                text = 'error'
            else:
                text = 'x:{0:6g}, y:{1:6g}'.format(shiftX, shiftY)
                if shiftH5.is_snapped_to_grid() and gridShift is not None:
                    text += ', grid:{0}'.format(gridShift)

        self._setDataInternal(IntensityModelColumns.ShiftColumn, text)


class TotalIntensityViewItemNode(IntensityViewItemNode):
    checkable = False
    className = 'Total'

    item = property(lambda self: self.subject.getIntensityItem('Total'))

    def scatterData(self):
        raise NotImplementedError('')
        # # TODO : this is not very satisfactory...
        # if not self.__shiftItem:
        #     return self.item.getScatterData() + (None,)
        #
        # shiftH5 = self.__shiftItem.shiftH5
        # with shiftH5:
        #     shift = shiftH5.shift(self.item.entry)
        #     if shift['shift_x'] is not None and shift['shift_y'] is not None:
        #         shift_idx = shiftH5.shifted_indices(self.item.entry)
        #     else:
        #         shift_idx = None
        #
        # return data + (shift_idx,)


class IntensityGroupNode(Node):
    """
    Node displaying info about the number of entries selected.
    """
    # groupClasses = [('Total', TotalIntensityViewItemNode)]

    total = property(lambda self: self.__total)
    samplePos = property(lambda self: self.__samplePos)
    icons = None

    def __init__(self, iGroupItem, checkableChildren=True, **kwargs):
        """
        Node representing the total intensity. Its children
        are the intensity for each angle.
        :param iGroupItem: this nodes IntensityGroupItem
        :param checkableChildren: if set to True (default) the angle
        intensity nodes will be checkable.
        :param kwargs:
        """
        super(IntensityGroupNode, self).__init__(subject=iGroupItem, **kwargs)
        # TODO : check item type
        self.nodeName = 'Default'
        self.__total = None
        self.__samplePos = None
        self.__notifyModel = True
        self.__checkableChildren = checkableChildren
        self.__shiftItem = None

    def _loadChildren(self):
        iGroup = self.subject
        iItems = iGroup.getIntensityItems()

        children = []
        for iItem in iItems:
            if iItem.entry == 'Total':
                continue
            iNode = IntensityViewItemNode(iItem)
            iNode.checkable = self.__checkableChildren
            if iNode.checkable:
                iNode.setCheckState(Qt.Qt.Checked)
            iNode.setShiftItem(self.__shiftItem)
            children.append(iNode)
        self.nodeName = 'Total {0}/{0}'.format(len(children))
        return children

    def _childInternalDataChanged(self, sender, *args):
        super(IntensityGroupNode, self)._childInternalDataChanged(sender,
                                                                  *args)
        if sender.parent() != self:
            return

        if self.__notifyModel:
            # self.__getTotal()
            self.__total = None
            self.__samplePos = None
            self.__updateNodeName()
            self.sigInternalDataChanged.emit([0])

    def __updateNodeName(self):
        """
        Updates this node's name
        :return:
        """
        entries, selected, _ = self.getCheckedEntries()
        self.nodeName = 'Intensity ({0} / {1})'.format(len(selected),
                                                       len(entries))

    def __getTotal(self):
        total = None
        samplePos = None
        nSelected = 0

        childCount = self.childCount() - len(self.groupClasses)

        n_ignore = 0

        for childIdx in range(childCount):
            child = self.child(childIdx)

            if isinstance(child, TotalIntensityViewItemNode):
                continue
            if (child.checkState == Qt.Qt.Unchecked
                    or isinstance(child, TotalIntensityViewItemNode)):
                continue
            if not child.checkable:
                n_ignore += 1
                continue

            nSelected += 1

            intensity, pos = child.scatterData()
            if total is None:
                total = intensity
                samplePos = pos
            else:
                total += intensity

        blocked = self.blockSignals(True)
        self.nodeName = 'Intensity ({0} / {1})'.format(nSelected,
                                                       childCount - n_ignore)
        self.__total = total
        self.__samplePos = samplePos
        self.blockSignals(blocked)

    def scatterData(self):
        if self.total is None:
            self.__getTotal()
        return self.total, self.samplePos

    def getCheckedEntries(self):
        """
        Returns the list of entries, the list of checked entries indices,
        and the list of unchecked entries indices.
        :return:
        """
        selected = []
        unselected = []
        entries = []
        index = -1
        for childIdx in range(self.childCount()):
            child = self.child(childIdx)

            if not child.checkable:
                continue

            # only the actual entries are supposed to be checkable,
            # not 'Total'
            index += 1

            entries.append(child.item.entry)

            if child.checkState == Qt.Qt.Unchecked:
                unselected.append(index)
            else:
                selected.append(index)

        return entries, selected, unselected

    def checkAll(self):
        """
        Check all entries.
        :return:
        """
        # blocked = self.blockSignals(True)
        self.__notifyModel = False
        for childIdx in range(self.childCount()):
            child = self.child(childIdx)
            if child.checkable:
                child.setCheckState(Qt.Qt.Checked)
        # self.blockSignals(blocked)
        self.__getTotal()
        self.__notifyModel = True
        self._notifyDataChange()

    def uncheckAll(self):
        """
        Uncheck all entries.
        :return:
        """
        # blocked = self.blockSignals(True)
        self.__notifyModel = False
        for childIdx in range(self.childCount()):
            child = self.child(childIdx)
            if child.checkable:
                child.setCheckState(Qt.Qt.Unchecked)
        # self.blockSignals(blocked)
        self.__getTotal()
        self.__notifyModel = True
        self._notifyDataChange()

    def setShiftItem(self, shiftItem=None):
        """
        Sets the shift item to be used by this node and its children.
        The contents of the items are not checked.
        :param shiftItem: an instance of ShiftItem, or None to clear the
        existing one.
        :return:
        """
        self.__shiftItem = shiftItem
        self.__total = None
        self.__samplePos = None
        children = self._children(initialize=False)
        for child in children:
            if isinstance(child, IntensityViewItemNode):
                blocked = child.blockSignals(True)
                child.setShiftItem(shiftItem)
                child.blockSignals(blocked)
        self._notifyDataChange()
#
#
# class ShiftedIntensityViewItemNode(IntensityViewItemNode):
#     pass
#     # checkable = True
#     #
#     # item = property(lambda self: self.__item)
#     #
#     # def __init__(self, iItem, **kwargs):
#     #     super(IntensityViewItemNode, self).__init__(**kwargs)
#     #     # TODO : check item type
#     #     self.__item = iItem
#     #     shortName = iItem.projectRoot().shortName(iItem.entry)
#     #     if not shortName:
#     #         shortName = iItem.path.split('/')[-1]
#     #     self.nodeName = str(shortName)
#     #
#     #     self._setDataInternal(IntensityModelColumns.AngleColumn,
#     #                           iItem.entry,
#     #                           Qt.Qt.ToolTipRole)
#     #
#     # def scatterData(self):
#     #     return self.item.getScatterData()
#
#
# class ShiftedTotalIntensityViewItemNode(TotalIntensityViewItemNode):
#     pass
#     # checkable = False
#     # className = 'Total'
#     #
#     # item = property(lambda self: self.subject.getIntensityItem('Total'))
#     #
#     # def scatterData(self):
#     #     with self.subject:
#     #         return self.subject.getIntensityItem('Total').getScatterData()

#
# class ShiftedIntensityGroupNode(IntensityGroupNode):
#     totalClass = ShiftedTotalIntensityViewItemNode
#     childClass = ShiftedIntensityViewItemNode


class IntensityModelColumns(object):
    AngleColumn, ShiftColumn = 0, 1
    ColumnNames = ['Angle', 'Shift']


class IntensityRootNode(RootNode):
    """
    Root node for the FitModel
    """
    ColumnNames = IntensityModelColumns.ColumnNames


class IntensityModel(Model):
    """
    Model displaying a FitH5 file contents.
    """
    RootNode = IntensityRootNode
    ModelColumns = IntensityModelColumns
    ColumnsWithDelegates = None

    def __init__(self,
                 intensityGroupItem,
                 checkableChildren=True,
                 **kwargs):
        super(IntensityModel, self).__init__(**kwargs)
        iGroupNode = IntensityGroupNode(intensityGroupItem,
                                        checkableChildren=checkableChildren)
        self.appendGroup(iGroupNode)

        # TODO : implement a search method that accepts a node type
        self.__iGroupNode = iGroupNode

    def __iTotalNode(self):
        iTotalIndex = self.index(0, 0)
        return iTotalIndex.data(ModelRoles.InternalDataRole)

    def getCheckedEntries(self):
        iTotalNode = self.__iTotalNode()
        if iTotalNode is None:
            return []
        return iTotalNode.getCheckedEntries()

    def checkAll(self):
        iTotalNode = self.__iTotalNode()
        if iTotalNode is None:
            return []
        iTotalNode.checkAll()

    def uncheckAll(self):
        iTotalNode = self.__iTotalNode()
        if iTotalNode is None:
            return []
        iTotalNode.uncheckAll()

    def setShiftItem(self, shiftItem=None):
        """
        Sets the shift item to be used by this model.
        The contents of the items are not checked.
        :param shiftItem: an instance of ShiftItem, or None to clear the
        existing one.
        :return:
        """
        self.__iGroupNode.setShiftItem(shiftItem)

    def iGroupNodeIndex(self):
        """
        Returns the index of the Top level node (IntensityGroupNode).
        :return:
        """
        return self.index(0, 0)


class IntensityTree(TreeView):
    sigCurrentChanged = Qt.Signal(object)

    def __init__(self, intensityGroupItem,
                 checkable=True,
                 **kwargs):
        """
        A tree linked to the summed intensity of an acquisition
        (IntensityGroup project item).
        :param intensityGroupItem:
        :param checkable: if True (default) the individual nodes will be
        checkable.
        :param kwargs:
        """
        super(IntensityTree, self).__init__(**kwargs)

        model = IntensityModel(intensityGroupItem,
                               checkableChildren=checkable)

        self.setModel(model)
        self.setShowUniqueGroup(True)
        rootIndex = self.model().index(0, 0)
        self.setExpanded(rootIndex, True)
        model.startModel()

    def currentChanged(self, current, previous):
        super(IntensityTree, self).currentChanged(current, previous)
        node = current.data(ModelRoles.InternalDataRole)
        if not node:
            return
        self.sigCurrentChanged.emit(node)


if __name__ == '__main__':
    pass
